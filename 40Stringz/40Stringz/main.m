//
//  main.m
//  40Stringz
//
//  Created by Hector Sanchez on 10/23/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{
    @autoreleasepool {
        //Documents path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName = [NSString stringWithFormat:@"%@/cool.txt", documentsDirectory];
        
        
        //Create string to write
        NSMutableString *str = [[NSMutableString alloc] init];
        for (int i=0; i<10; i++) {
            [str appendString:@"Chip´s cool! \n"];            
        }
        
        //Declare NSError
        NSError *error = nil;        
        
        //Write file with NSError passed by reference
        BOOL success = [str writeToFile:fileName atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
        //Log
        if (success) {
            NSLog(@"File written to:  %@", documentsDirectory);
        }else{
            NSLog(@"File not written to: %@", documentsDirectory);
        }
        
    }
    return 0;
}

