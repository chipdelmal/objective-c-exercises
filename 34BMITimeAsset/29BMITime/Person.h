//
//  Person.h
//  29BMITime
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
{
    //Instance variables
    float heightInMeters;
    int weightInKilos;
}

@property float heightInMeters;
@property int weightInKilos;

//Operation methods
-(float) calculateBodyMassIndex;



/*//Setter methods
-(void) setHeightInMeters:(float)h;
-(void) setWeightInKilos:(float)w;

//Getter methods
-(float) heightInMeters;
-(float) weightInKilos;*/

@end
