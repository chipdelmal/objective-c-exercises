//
//  main.m
//  29BMITime
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"
#import "Asset.h"

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSMutableArray *employees=[[NSMutableArray alloc] init];
    
    for (int i=0; i<10; i++) {
        Employee *person = [[Employee alloc] init];
        [person setHeightInMeters:(1.70+i*.1)];
        [person setWeightInKilos:90 + i];
        [person setEmployeeID:i];        
        [employees addObject:person];
    }
    
    for(int j=0; j<10; j++){
        Asset *asset=[[Asset alloc] init];
        NSString *label=[NSString stringWithFormat: @"Laptop: %d", j];
        [asset setLabel:label];
        [asset setResaleValue:j*17];
        
        NSUInteger randomIndex = random() % [employees count];
        
        [[employees objectAtIndex:randomIndex] addAssetsObject: asset];
    }
    
    NSLog(@"Employees: %@", employees);    
    NSLog(@"Giving up ownership of one employee");    
    [employees removeObjectAtIndex:5];    
    NSLog(@"Giving up ownership of array");    
    employees = nil;

    [pool drain];
    return 0;
}

