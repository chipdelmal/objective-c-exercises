//
//  StockHolding.m
//  31Stocks
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "StockHolding.h"

@implementation StockHolding

@synthesize currentSharePrice, purchaseSharePrice, numberOfShares;

- (float)costInDollars
{
    return [self purchaseSharePrice] * [self numberOfShares];
}
- (float)valueInDollars 
{
    return [self currentSharePrice] * [self numberOfShares];
}
- (void)setNumberOfShares:(int)shares andPurchaseSharePrice:(float)purchase andCurrentSharePrice:(float)current
{
    [self setNumberOfShares:shares];
    [self setPurchaseSharePrice:purchase];
    [self setCurrentSharePrice:current];
}

@end
