//
//  main.c
//  09Numbers
//
//  Created by Hector Sanchez on 10/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
int main (int argc, const char * argv[])
{
    int x = 255;
    printf("x is %d.\n", x);
    printf("In octal, x is %o.\n", x);
    printf("In hexadecimal, x is %x.\n", x);

    long y = 255;
    printf("x is %ld.\n", y);
    printf("In octal, x is %lo.\n", y);
    printf("In hexadecimal, x is %lx.\n", y);

    unsigned long z = 255;
    printf("x is %lu.\n", z); 
    // Octal and hex already assumed the number was unsigned
    printf("In octal, x is %lo.\n", z);
    printf("In hexadecimal, x is %lx.\n", z);
    
    
    printf("3 * 3 + 5 * 2 = %d\n", 3 * 3 + 5 * 2);
    printf("11 / 3 = %d\n", 11 / 3);
    printf("11 / 3 = %d remainder of %d \n", 11 / 3, 11 % 3);
    printf("11 / 3.0 = %f\n", 11 / (float)3);
    
    printf("The absolute value of -5 is %d\n", abs(-5));
    
    
    double a = 12345.6789;
    printf("y is %.2f\n", a);
    printf("y is %.2e\n", a);
    
    return 0;
    
    return 0;
}

