//
//  main.m
//  21SecondsAlive
//
//  Created by Hector Sanchez on 10/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    //Create date components to be used by NSCalendar object
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:1986];
    [components setMonth:5];
    [components setDay:2];
    [components setHour:12];
    [components setMinute:22];
    [components setSecond:1];
    
    //Create NSCalendar object that will yield the NSDate object with components
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *dateOfBirth = [calendar dateFromComponents: components];
    
    //Create NSDate object with current date
    NSDate *now = [NSDate date];
    
    //Create float object with the difference of seconds between now and dateOfBirth
    double seconds = [now timeIntervalSinceDate:dateOfBirth];
    
    //Log
    NSLog(@"The elapsed seconds I've been alive are: %.0f", seconds);
    
    
    [pool drain];
    return 0;
}

