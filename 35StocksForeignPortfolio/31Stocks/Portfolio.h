//
//  Portfolio.h
//  31Stocks
//
//  Created by Hector Sanchez on 10/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StockHolding.h"

@interface Portfolio : NSObject
{
    int portfolioID;
    NSMutableArray *stocks;
}
-(void)addStockObject:(StockHolding *)addedStock;
-(unsigned int)stocksValue; 


@property (strong) NSMutableArray *stocks;
@property int portfolioID;
@end
