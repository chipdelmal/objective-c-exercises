//
//  main.m
//  31Stocks
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <stdlib.h>
#import "ForeignStockHolding.h"
#import "Portfolio.h"

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    NSMutableArray *portfoliosArray = [[NSMutableArray alloc] init];
    
    //Create portfolios
    for (int j=0; j<5; j++) {
        Portfolio *tempPortfolio = [[Portfolio alloc] init];
        [tempPortfolio setPortfolioID:j];
        //Create stocks
        for (int i=0; i<10; i++) {
            StockHolding *tempStock = [[StockHolding alloc] init];
            [tempStock setNumberOfShares:arc4random()%10];
            [tempStock setCurrentSharePrice:arc4random()%1000/10.0];
            [tempStock setPurchaseSharePrice:arc4random()%1000/10.0];
            [tempStock setStockID:rand()%10];
            [tempPortfolio addStockObject:tempStock];
        }
        [portfoliosArray addObject:tempPortfolio];
    }
    
    NSLog(@"Portfolios: %@", portfoliosArray); 

    [pool drain];
    return 0;
}

