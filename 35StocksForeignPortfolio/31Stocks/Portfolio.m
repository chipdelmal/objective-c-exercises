//
//  Portfolio.m
//  31Stocks
//
//  Created by Hector Sanchez on 10/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Portfolio.h"

@implementation Portfolio

-(void)addStockObject:(StockHolding *)addedStock
{
    if(!stocks){
        stocks=[[NSMutableArray alloc] init];
    }
    [stocks addObject:addedStock];
}

-(unsigned int)stocksValue
{
    float value=0;
    for(int i=0; i<[stocks count]; i++){
        value=value+[[stocks objectAtIndex:i] valueInDollars];
    }
    return value; 
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<Portfolio %d: $%d in assets>",[self portfolioID], [self stocksValue]];
}

@synthesize stocks,portfolioID;
@end
