//
//  StockHolding.h
//  31Stocks
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockHolding : NSObject
{
    float   purchaseSharePrice;
    float   currentSharePrice;
    int     numberOfShares;
    int     stockID;
}
@property float purchaseSharePrice;
@property float currentSharePrice;
@property int numberOfShares;
@property int stockID;

- (float)costInDollars; 
- (float)valueInDollars; 
- (void)setNumberOfShares:(int)shares andPurchaseSharePrice:(float)purchase andCurrentSharePrice:(float)current;

@end
