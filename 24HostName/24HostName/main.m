//
//  main.m
//  24HostName
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSHost *computerHost = [[NSHost alloc] init];
    NSString *hostName = [computerHost localizedName];
    
    NSLog(hostName);

    [pool drain];
    return 0;
}

