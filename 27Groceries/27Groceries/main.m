//
//  main.m
//  27Groceries
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSMutableArray *groceries = [[NSMutableArray alloc] init];
    
    [groceries addObject:@"milk"];
    [groceries addObject:@"bread"];
    [groceries insertObject:@"lettuce" atIndex:0];
    
    for(int i=0; i<[groceries count];i++) {
        NSLog(@"%@", [groceries objectAtIndex:i]);
    }
    

    [pool drain];
    return 0;
}

