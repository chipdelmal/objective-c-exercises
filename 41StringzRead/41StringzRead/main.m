//
//  main.m
//  41StringzRead
//
//  Created by Hector Sanchez on 10/23/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        //Create NSError
        NSError *error = nil;
        NSString *str = [[NSString alloc] initWithContentsOfFile:@"/etc/resolv.conf" encoding:NSASCIIStringEncoding error:&error];
        
        //Log
        if (error) {
            NSLog(@"There was an error reading file: %@",[error description]);
        }else{
            NSLog(@"%@",str);
        }
        
    }
    return 0;
}

