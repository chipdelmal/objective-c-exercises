//
//  Person.m
//  30Self
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize heightInMeters, weightInKilos;

//Operation Methods
-(float) calculateBodyMassIndex
{
    return [self weightInKilos] / ([self heightInMeters] * [self heightInMeters]);
}

- (void)addYourselfToArray:(NSMutableArray *)theArray
{
    [theArray addObject:self];
}

@end
