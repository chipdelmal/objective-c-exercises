//
//  Person.h
//  30Self
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
{
    //Instance variables
    float heightInMeters;
    int weightInKilos;
}

@property float heightInMeters;
@property int weightInKilos;

//Operation methods
-(float) calculateBodyMassIndex;
-(void)addYourselfToArray:(NSMutableArray *)theArray;

@end
