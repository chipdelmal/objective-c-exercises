//
//  main.c
//  18TheHeap
//
//  Created by Hector Sanchez on 10/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>



int main (int argc, const char * argv[])
{
    float *startOfBuffer;
    
    //Assign 1000 float spaces to start of buffer
    startOfBuffer = malloc(1000 * sizeof(float));
    
    //Use buffer    
    
    //Release buffer and forget direction
    free(startOfBuffer);
    startOfBuffer = NULL;
    
    return 0;
}

