//
//  main.m
//  46Appliances
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appliance.h"

int main (int argc, const char * argv[])
{

    @autoreleasepool {     
        Appliance *a = [[Appliance alloc] init];
        NSLog(@"%@",a);
        //If the object doesn't have a setProductName it will access the variable directly
        [a setValue:@"Washing Machine" forKey:@"productName"];
        //[a setVoltage:240];
        [a setValue:[NSNumber numberWithInt:240] forKey:@"voltage"];
        NSLog(@"%@",a);
        //If there is no productName method the variable will be accessed directly
        NSLog(@"the product name is %@", [a valueForKey:@"productName"]);
    }
    return 0;
}

