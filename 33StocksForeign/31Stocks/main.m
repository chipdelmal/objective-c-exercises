//
//  main.m
//  31Stocks
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <stdlib.h>
#import "ForeignStockHolding.h"

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    //Create array to hold the stocks information
    NSMutableArray *stockArray = [[NSMutableArray alloc] init];
    
    //Fill the array with stocks
    for (int i=0; i<3; i++) 
    {
        //Create a temporary stock
        ForeignStockHolding *tempStock = [[ForeignStockHolding alloc] init];
        [tempStock setConversionRate:(arc4random() % 10)/100.];
        float randPurchasePrice = (arc4random() % 1000)/10.0;
        float randCurrentPrice = (arc4random() % 1000)/10.0;
        int randStocksNumber = arc4random() % 100;
        //Fill the stock with random values
        [tempStock setNumberOfShares:randStocksNumber andPurchaseSharePrice:randCurrentPrice andCurrentSharePrice:randPurchasePrice];
        //Add the stock to the Array
        [stockArray addObject:tempStock];
    }
        
    //Print the array's operations
    for (int j=0; j<3; j++)
    {
        NSLog(@"Cost in dollars: %.2f; Value in dollars: %.2f",[[stockArray objectAtIndex:j] costInDollars],[[stockArray objectAtIndex:j] valueInDollars]);
    }
    

    [pool drain];
    return 0;
}

