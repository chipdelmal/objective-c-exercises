//
//  ForeignStockHolding.m
//  31Stocks
//
//  Created by Hector Sanchez on 10/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ForeignStockHolding.h"

@implementation ForeignStockHolding

- (float)costInDollars
{
    //Use superclass' function and multiply by class' conversionRate
    return [super costInDollars]*[self conversionRate];
}
- (float)valueInDollars 
{
    //Use superclass' function and multiply by class' conversionRate
    return [super valueInDollars]*[self conversionRate];
}

@synthesize conversionRate;
@end
