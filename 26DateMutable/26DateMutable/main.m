//
//  main.m
//  26DateMutable
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSDate *today = [NSDate date];
    NSDate *tomorrow = [today dateByAddingTimeInterval: 86400];
    NSDate *yesterday = [today dateByAddingTimeInterval:-86400];
    
    NSMutableArray *datesArray = [[NSMutableArray alloc] init];
    
    [datesArray addObject:today];
    [datesArray addObject:tomorrow];
    [datesArray insertObject:yesterday atIndex:0];
    
    for(int i=0; i<[datesArray count];i++){
        NSLog(@"%@", [datesArray objectAtIndex:i]);
    }

    [pool drain];
    return 0;
}

