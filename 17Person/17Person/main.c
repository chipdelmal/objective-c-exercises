//
//  main.c
//  17Person
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <math.h>

typedef struct
{
    float height;
    float weight;
}Person;

float bodyMassIndex(Person p);

int main (int argc, const char * argv[])
{
    Person chip;
    chip.height=1.73;
    chip.weight=64;
    
    printf("Chip's weight and height are: %.2fkg and %.2fm\n",chip.weight,chip.height);
    printf("Chip's Body Mass Index is: %.2f\n", bodyMassIndex(chip));
    
    return 0;
}


float bodyMassIndex(Person p)
{
    return p.weight/(powf(p.height,2));
}