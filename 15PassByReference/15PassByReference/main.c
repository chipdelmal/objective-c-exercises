//
//  main.c
//  15PassByReference
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main (int argc, const char * argv[])
{
    double pi = 3.14;
    double integerPart;
    double fractionPart;
    
    //Send integerPart as reference and assign return value to fraction part
    fractionPart = modf(pi, &integerPart);
    
    printf("fractionPart= %.0f and integerPart= %.2f \n", integerPart, fractionPart);
    
    return 0;
}

