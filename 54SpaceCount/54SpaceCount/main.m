//
//  main.m
//  54SpaceCount
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int spaceCount(char *sentence);

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        char *sentence = "He was not in the cab at the time.";
        printf("%d spaces",spaceCount(sentence));
    }
    return 0;
}

int spaceCount(char *sentence)
{
    BOOL    stop=FALSE;
    int     index = 0;
    int     count = 0;
    
    while (stop==FALSE) {
        if (sentence[index]==0x20) {
            count++;
        }
        if (sentence[index]=='\0') {
            stop=TRUE;
        }
        index++;
    }
    return count;
}