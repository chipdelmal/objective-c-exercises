//
//  main.m
//  25NSArray
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSDate *today = [NSDate date];
    NSDate *yesterday = [today dateByAddingTimeInterval:86400];
    NSDate *tomorrow = [today dateByAddingTimeInterval:-86400];
    
    NSArray *datesArray = [NSArray arrayWithObjects:today, tomorrow, yesterday, nil];
    
    for(int i=0; i<[datesArray count]; i++){
        NSLog(@"%@", [datesArray objectAtIndex:i]);
    }
    
    for (NSDate *d in datesArray) {
        NSLog(@"Here is a date: %@", d);
    }

    [pool drain];
    return 0;
}

