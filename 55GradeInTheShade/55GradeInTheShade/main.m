//
//  main.m
//  55GradeInTheShade
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <stdlib.h>

float averageFloats(float data[]/*float *data*/, int dataCount)
{
    float sum = 0.0;
    for (int i=0;i<dataCount; i++) {
        sum = sum + data[i];
    }
    return sum/dataCount;
}


int main (int argc, const char * argv[])
{
    // Declares the array as part of the frame
    //float gradeBook[3];
    float gradeBook[] = {60.2, 94.5, 81.1};

    //float *gradeBook = malloc(3*sizeof(float));
    //gradeBook[0]=60.2;
    //gradeBook[1]=94.5;
    //gradeBook[2]=81.1;
        
    float average = averageFloats(gradeBook, 3);
        
    //free(gradeBook);
    //gradeBook=NULL;
        
    printf("Average = %.2f\n",average);
    
    return 0;
}

