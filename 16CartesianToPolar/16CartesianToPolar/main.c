//
//  main.c
//  16CartesianToPolar
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <math.h>


void cartesianToPolar(float x, float y, double *rPtr, double *thetaPtr);

int main (int argc, const char * argv[])
{
    double x = 3;
    double y = 4;
    double radius;
    double angle;
    
    cartesianToPolar(x, y, &radius, &angle);
    printf("(%.2f,%.2f) becomes (%.2f,%.2f) \n", x, y, angle, radius);
    
    return 0;
}

void cartesianToPolar(float x, float y, double *rPtr, double *thetaPtr)
{
    if (rPtr||thetaPtr) 
    {
        return;
    }

    *rPtr = sqrtf(powf(x,2)+powf(y,2));

    
    
    if (x==0) 
    {
        if (y==0) 
        {
            *thetaPtr = 0;
        }
        else if (y>0)
        {
            *thetaPtr = M_PI_2;
        }
        else
        {
            *thetaPtr = -M_PI_2;
        }
    }
    else
    {
        *thetaPtr = atanf(y/x);
    }
}