//
//  main.c
//  10Sine
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main (int argc, const char * argv[])
{
    float radianAngle=1;
    float result;
    
    result = sin(radianAngle);
    
    printf("The Sine of %.2f is: %.2f \n", radianAngle, result);
}

