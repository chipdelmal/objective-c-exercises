//
//  Employee.h
//  29BMITime
//
//  Created by Hector Sanchez on 10/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Person.h"
@class Asset;

@interface Employee : Person
{
    int employeeID;
    NSMutableSet *assets;
}
@property int employeeID;
- (void)addAssetsObject:(Asset *)a;
- (unsigned int)valueOfAssets;

@end