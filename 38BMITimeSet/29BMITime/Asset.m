//
//  Asset.m
//  29BMITime
//
//  Created by Hector Sanchez on 10/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Asset.h"

@implementation Asset

@synthesize label, resaleValue, holder;

- (NSString *)description
{
    if ([self holder]) {
        return [NSString stringWithFormat:@"<%@: $%d, assigned to %@>",[self label],[self resaleValue], [self holder]];
    }else{
        return [NSString stringWithFormat:@"<%@: $%d >",
                [self label], [self resaleValue]];        
    }
    
}

- (void)dealloc
{
    NSLog(@"deallocating %@", self);
}

@end