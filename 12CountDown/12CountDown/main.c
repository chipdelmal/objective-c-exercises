//
//  main.c
//  12CountDown
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>

int main (int argc, const char * argv[])
{
    int i;
    
    for(i=99; i>0; i=i-3)
    {
        if (i % 5 == 0) 
        {
            printf("%d Found one! \n", i);
            continue;
        }
        printf("%d \n",i);
    }
    return 0;
}

