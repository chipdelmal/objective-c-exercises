//
//  main.m
//  23Strings
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSString *lament = @"Why me?";
    NSString *other = [NSString stringWithFormat:@"2 + 2 = @d", 5];
    
    BOOL x = [lament isEqualToString:other];

    if (x) {
        NSLog(@"They are equal");
    }else{
        NSLog(@"They are not equal");
    }
    NSLog(lament);
    
    [pool drain];
    return 0;
}

