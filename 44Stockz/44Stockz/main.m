//
//  main.m
//  44Stockz
//
//  Created by Hector Sanchez on 10/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSMutableArray          *stocks = [[NSMutableArray alloc] init];
        NSMutableDictionary     *stock;
        
        //Write
        stock = [NSMutableDictionary dictionary];
        [stock setObject:@"AAPL" forKey:@"symbol"];
        [stock setObject:[NSNumber numberWithInt:200] forKey:@"shares"];
        [stocks addObject:stock];
        
        stock = [NSMutableDictionary dictionary];
        [stock setObject:@"GOOG" forKey:@"symbol"];
        [stock setObject:[NSNumber numberWithInt:160] forKey:@"shares"];
        [stocks addObject:stock];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName = [NSString stringWithFormat:@"%@/cool.plist", documentsDirectory];
        
        [stocks writeToFile:fileName atomically:YES];
        
        //Read
        NSArray *stockList = [NSArray arrayWithContentsOfFile:fileName];
        
        for (NSDictionary *d in stockList) {
            NSLog(@"I have %@ shares of %@",[d objectForKey:@"shares"], [d objectForKey:@"symbol"]);
        }
        
        
    }
    return 0;
}