//
//  main.m
//  20TimeAfterTime
//
//  Created by Hector Sanchez on 10/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Create pointer to a new NSDate object
    NSDate *now = [[NSDate alloc] init];
    
    //Print direction of the pointer and date object
    NSLog(@"The new date lives at %p", now);
    NSLog(@"The new date is %@", now);
    
    //Get time elapsed from 1970
    double seconds=[now timeIntervalSince1970];
    NSLog(@"%f seconds have passed since 1970", seconds);
    
    //Create new NSDate object and ad 100000 seconds
    NSDate *later = [now dateByAddingTimeInterval:100000];
    NSLog(@"In 100,000 seconds it will be %@", later);
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSUInteger day = [cal ordinalityOfUnit:NSDayCalendarUnit
                                    inUnit:NSMonthCalendarUnit
                                   forDate:now];
    NSLog(@"This is day %lu of the month", day);
    
    [pool drain];
    return 0;
}

