//
//  BNRDocument.h
//  TahDoodle
//
//  Created by Hector Sanchez on 10/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BNRDocument : NSDocument
{
    NSMutableArray              *todoItems;
    IBOutlet    NSTableView     *itemTableView;
}
-(IBAction)createNewItem:(id)sender;
-(IBAction)deleteItem:(id)sender;

@end
