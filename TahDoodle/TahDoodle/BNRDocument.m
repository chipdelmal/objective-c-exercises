//
//  BNRDocument.m
//  TahDoodle
//
//  Created by Hector Sanchez on 10/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BNRDocument.h"

@implementation BNRDocument

# pragma mark - NSDocument Overrides

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
        // If an error occurs here, return nil.
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"BNRDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    //Method opened when document is saved
    
    //Create a blank array if there is none
    if(!todoItems){
        todoItems = [NSMutableArray array];
    }
    
    //Create NSData object from list
    NSData *data = [NSPropertyListSerialization dataWithPropertyList:todoItems format:NSPropertyListXMLFormat_v1_0 options:NSPropertyListMutableContainers error:outError]; 
    return data;
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    //Method opened when document is loaded
    
    //Extract data from object
    todoItems = [NSPropertyListSerialization propertyListWithData:data options:0 format:NULL error:outError];
    return (todoItems != nil);
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

# pragma mark - Actions

-(IBAction)createNewItem:(id)sender
{
    //If there is no array create one
    if (!todoItems) {
        todoItems = [NSMutableArray array];
    }
    
    //Add new object and refresh viewed table
    [todoItems addObject:@"New Object"];
    [itemTableView reloadData];
    //Flag changes in app
    [self updateChangeCount:NSChangeDone];
    
}

-(IBAction)deleteItem:(id)sender
{
    if (!todoItems) {
        todoItems = [NSMutableArray array];
    }else{
        [todoItems removeObjectAtIndex:[itemTableView selectedRow]];
        [itemTableView reloadData];
        [self updateChangeCount:NSChangeDone];
    }

}

#pragma mark Data Source Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tv
{
    //Set number of rows in the table
    return [todoItems count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    //Return the object to be displayed in each cell
    return [todoItems objectAtIndex:row];
}

- (void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    //Refresh table on change and flag for save
    [todoItems replaceObjectAtIndex:row withObject:object];
    [self updateChangeCount:NSChangeDone];
}
@end
