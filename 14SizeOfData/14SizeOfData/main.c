//
//  main.c
//  14SizeOfData
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>


int main (int argc, const char * argv[])
{
    printf("Sizes of:\n");
    printf("int:\t %zu\n",sizeof(int));
    printf("float:\t %zu\n", sizeof(float));
    printf("short:\t %zu\n", sizeof(short));
    printf("long:\t %zu\n", sizeof(long));
    printf("double:\t %zu\n", sizeof(double));
    
    return 0;
}


