//
//  main.c
//  51integerOtherBit
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>

int main (int argc, const char * argv[])
{
    unsigned long b = 0x01;
    for(int i=0;i<32;i++){
        b = b << 2;
        b = b | 0x01;
    }
    printf("%lu\n",b);
}

