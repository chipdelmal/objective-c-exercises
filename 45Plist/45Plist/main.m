//
//  main.m
//  45Plist
//
//  Created by Hector Sanchez on 10/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSMutableArray          *magicArray = [[NSMutableArray alloc] init];
        NSMutableDictionary     *magicDictionary;
        
        magicDictionary = [[NSMutableDictionary alloc] init];
        [magicDictionary setObject:[NSNumber numberWithInt:rand()%2] forKey:@"Int"];
        [magicDictionary setObject:[NSNumber numberWithFloat:rand()%10/10] forKey:@"Float"];
        [magicDictionary setObject:@"test" forKey:@"String"];
        
        [magicArray addObject:magicDictionary];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName = [NSString stringWithFormat:@"%@/challenge.plist", documentsDirectory];
        
        [magicArray writeToFile:fileName atomically:YES];
    }
    return 0;
}

