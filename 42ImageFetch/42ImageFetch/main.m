//
//  main.m
//  42ImageFetch
//
//  Created by Hector Sanchez on 10/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        //Fetch URL Data
        NSURL *url = [NSURL URLWithString:@"http://www.google.com/images/logos/ps_logo2.png"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:NULL error:&error];
        
        //Log fetching
        if(!data){
            NSLog(@"Fetch failed: %@", [error localizedDescription]);
        }
        NSLog(@"The file is %lu bytes", [data length]);
        
        //Documents path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName = [NSString stringWithFormat:@"%@/google.png", documentsDirectory];
        
        //Write and log
        BOOL written = [data writeToFile:fileName options:NSDataWritingAtomic error:&error];
        if (!written) {
            NSLog(@"write failed: %@",[error localizedDescription]);
        }        
        NSLog(@"Success");
        
        //Read and log
        NSData *readData = [NSData dataWithContentsOfFile:fileName];
        NSLog(@"The file read from the disk has %lu bytes", [readData length]);
    }
    return 0;
}

