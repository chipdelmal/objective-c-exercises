//
//  OwnedAppliance.m
//  46Appliances
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "OwnedAppliance.h"

@implementation OwnedAppliance

-(id)initWithProductName:(NSString *)pn firstOwnerName:(NSString *)n
{
    self = [super initWithProductName:pn];
    if(self){
        ownerNames = [[NSMutableSet alloc] init];
        if(n){
            [ownerNames addObject:n];
        }
    }
    return self;
}

- (id)initWithProductName:(NSString *)pn
{
    return [self initWithProductName:pn firstOwnerName:nil];
}

-(void)addOwnerNamesObject:(NSString *)n
{
    [ownerNames addObject:n];
}

-(void)removeOwnerNamesObject:(NSString *)n
{
    [ownerNames removeObject:n];
}

@end
