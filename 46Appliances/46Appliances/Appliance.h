//
//  Appliance.h
//  46Appliances
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Appliance : NSObject
{
    NSString *productName;
    int voltage;
}
@property (copy) NSString *productName;
@property int voltage;

// The designated initializer
-(id)initWithProductName:(NSString *)pn;

@end
