//
//  main.m
//  46Appliances
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appliance.h"

int main (int argc, const char * argv[])
{

    @autoreleasepool {     
        Appliance *a = [[Appliance alloc] init];
        NSLog(@"%@",a);
        [a setProductName:@"Washing Machine"];
        [a setVoltage:240];
        NSLog(@"%@",a);
    }
    return 0;
}

