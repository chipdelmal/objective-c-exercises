//
//  Appliance.m
//  46Appliances
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Appliance.h"

@implementation Appliance
@synthesize productName, voltage;

-(id)init
{
    return [self initWithProductName:@"Unknown"];
}

-(id)initWithProductName:(NSString *)pn
{
    self = [super init];
    if (self) {
        [self setProductName:pn];
        [self setVoltage:120];
    }
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %d volts>", productName, voltage];
}

@end
