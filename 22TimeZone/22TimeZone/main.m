//
//  main.m
//  22TimeZone
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSTimeZone *timeZoneNow = [[NSTimeZone alloc] init];
    BOOL daylightSaving = [timeZoneNow isDaylightSavingTime];
    
    if(daylightSaving){
        NSLog(@"Daylight Saving Time is ON");
    }else{
        NSLog(@"Daylight Saving Time is OFF");
    }

    [pool drain];
    return 0;
}

