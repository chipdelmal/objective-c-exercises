//
//  main.m
//  49VowelMovement
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        //Create array of strings to process
        NSArray *oldStrings = [NSArray arrayWithObjects:@"Sauerkraut", @"Raygun", @"Big Nerd Ranch", @"Mississippi", nil];
        NSLog(@"Old Strings: %@",oldStrings);
        //Create array to contain new ones
        NSMutableArray *newStrings = [NSMutableArray array];
        //Create list of removed characters
        NSArray *vowels = [NSArray arrayWithObjects:@"a", @"e", @"i", @"o", @"u", nil];
        
        
        //Declare block variable
        void (^devowelizer)(id, NSUInteger, BOOL *);
        //Assign a block to the variable
        devowelizer = ^(id string, NSUInteger i, BOOL *stop) {
            NSMutableString *newString = [NSMutableString stringWithString:string];
            for(NSString *s in vowels){
                NSRange fullRange = NSMakeRange(0, [newString length]);
                [newString replaceOccurrencesOfString:s withString:@"" options:NSCaseInsensitiveSearch range:fullRange];
            }
            [newStrings addObject:newString];
        };
        
        
        /*void (^devowelizer)(id, NSUInteger, BOOL *) = ^(id string, NSUInteger i, BOOL *stop) {
         
         NSMutableString *newString = [NSMutableString stringWithString:string];
         
         // Iterate over the array of vowels, replacing occurrences of each
         // with an empty string.
         for (NSString *s in vowels) {
         NSRange fullRange = NSMakeRange(0, [newString length]);
         [newString replaceOccurrencesOfString:s
         withString:@""
         options:NSCaseInsensitiveSearch
         range:fullRange];
         }
         
         [newStrings addObject:newString];
         };*/
        
        
        [oldStrings enumerateObjectsUsingBlock:devowelizer];
        NSLog(@"new strings: %@", newStrings);
        
    }
    return 0;
}

