//
//  main.c
//  52yostring
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, const char * argv[])
{
    char x = /*0x21*/ '!'; //character !
    while (x<=0x7e) {
        printf("%x is %c\n",x,x);
        x++;
    }
    
    //Pointer to 5 bytes on heap
    char *start = malloc(5);
    
    /**start = 'L';
    *(start + 1) = 'o';
    *(start + 2) = 'v';
    *(start + 3) = 'e';
    *(start + 4) = '\0';*/
    
    start[0] = 'L';
    start[1] = 'o';
    start[2] = 'v';
    start[3] = 'e';
    start[4] = '\0';
    
    // Print out the string and its length
    printf("%s has %zu characters\n", start, strlen(start));
    // Print out the third letter
    printf("The third letter is %c\n", *(start + 2));
    
    // Free the memory so that it can be reused
    free(start);
    start = NULL;
    
    
    const char *start2 = "Love";
    printf("%s has %zu characters\n", start2, strlen(start2));
    printf("The third letter is %c\n", start2[2]);
    
    
    
    // Claim a chunk of memory big enough to hold three floats
    float *favorites = malloc(3 * sizeof(float));
    
    // Push values into the locations in that buffer
    favorites[0] = 3.14158;
    favorites[1] = 2.71828;
    favorites[2] = 1.41421;
    
    // Print out each number on the list
    for (int i = 0; i < 3; i++) {
        printf("%.4f is favorite %d\n", favorites[i], i);
    }
    
    // Free the memory so that it can be reused
    free(favorites);
    favorites = NULL;

    
    return 0;
}

