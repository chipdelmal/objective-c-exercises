//
//  NSString+VowelCounting.h
//  48VowelCounter
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VowelCounting)
-(int)vowelCount;
@end
