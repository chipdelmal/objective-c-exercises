//
//  main.m
//  48VowelCounter
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+VowelCounting.h"

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        NSString *string = @"Hello World";
        NSLog(@"%@ has %d vowels", string, [string vowelCount]);
    }
    return 0;
}

