//
//  NSString+VowelCounting.m
//  48VowelCounter
//
//  Created by Hector Sanchez on 10/27/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NSString+VowelCounting.h"

@implementation NSString (VowelCounting)
-(int)vowelCount
{
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"awiouyAEIOUY"];
    NSUInteger count = [self length];
    int sum=0;
    for (int i = 0; i<count; i++) {
        unichar c = [self characterAtIndex:i];
        if([charSet characterIsMember:c]){
            sum++;
        }
    }
    return sum;
}

@end
