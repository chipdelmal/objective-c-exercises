//
//  main.c
//  11Loops
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>

int main (int argc, const char * argv[])
{
    int i=0;
    
    //While
    while (i<10) {
        printf("Chip's cool\n");
        i++;
    }    
    printf("\n\n");    
    
    //For
    for(i=0; i<10; i++){
        printf("Chip's cool\n");
    }
    printf("\n\n");    
    
    //Break
    for (i = 0; i < 12; i++) {
        printf("Checking i = %d\n", i);
        if (i + 90 == i * i) {
            break;
        }
    }
    printf("The answer is %d.\n\n", i);
    
    
    //Continue
    for (i = 0; i < 12; i++) {
        if (i % 3 == 0) {
            continue;
        }
        printf("Checking i = %d\n", i);
        if (i + 90 == i * i) {
            break;
        }
    }
    printf("The answer is %d.\n\n", i);
    
    //Do-While
    do {
        printf("%d. Aaron is Cool\n", i);
        i++;
    } while (i < 13);
    return 0;    
}

