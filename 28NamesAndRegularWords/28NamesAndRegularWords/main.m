//
//  main.m
//  28NamesAndRegularWords
//
//  Created by Hector Sanchez on 10/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    // Read proper names and regular words files
    NSString *nameString = [NSString stringWithContentsOfFile:@"/usr/share/dict/propernames"
                                                     encoding:NSUTF8StringEncoding
                                                        error:NULL];
    NSString *wordString = [NSString stringWithContentsOfFile:@"/usr/share/dict/words" 
                                                     encoding:NSUTF8StringEncoding 
                                                        error:NULL];
    
    //Separate into different strings in arrays
    NSArray *nameArray = [nameString componentsSeparatedByString:@"\n"];
    NSArray *wordArray = [wordString componentsSeparatedByString:@"\n"];
    
    //Cycle through every name
    for (int b=0; b<[nameArray count]; b++) 
    {
        NSLog(@"%@",[nameArray objectAtIndex:b]);
        
        //Cycle trough every word except the last one (this due to the fact that names that are also common words are repeated twice consecutively in the words list so if the last word is not repeated twice it is not a name and we are not interested in it)
        for (int a=0; a<[wordArray count]-1; a++) 
        {
            //When the name is found in the words dictionary it is marked as a potential match
            if (![[wordArray objectAtIndex:a] caseInsensitiveCompare:[nameArray objectAtIndex:b]]) 
            {
                //If the name appears twice consecutively in the words list it is also a name
                if(![[wordArray objectAtIndex:(a+1)] caseInsensitiveCompare:[nameArray objectAtIndex:b]])
                {
                    NSLog(@"%@",[wordArray objectAtIndex:a]);
                }
            }
        }
    }
    
    
  
    [pool drain];
    return 0;
}

