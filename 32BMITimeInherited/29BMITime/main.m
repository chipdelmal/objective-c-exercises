//
//  main.m
//  29BMITime
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Employee.h"

int main (int argc, const char * argv[])
{

    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    Employee *chip = [[Employee alloc] init];
    
    [chip setHeightInMeters:1.72];
    [chip setWeightInKilos:64];
    [chip setEmployeeID:466118];
    
    float bmi = [chip calculateBodyMassIndex];
    
    NSLog(@"The person's weight is: %d, it's height is: %.2f and the body mass is: %.2f", [chip weightInKilos], [chip heightInMeters], bmi);
    NSLog(@"The id is: %d",[chip employeeID]);

    [pool drain];
    return 0;
}

