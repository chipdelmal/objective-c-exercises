//
//  Employee.m
//  29BMITime
//
//  Created by Hector Sanchez on 10/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Employee.h"
@implementation Employee

-(float)calculateBodyMassIndex
{
    //Override the superclass's previous definition of calculateBodyMassIndex
    //return 19;
    
    //Use superclass' method calculateBodyMassIndex and use the result for other operation
    float normalBMI = [super calculateBodyMassIndex];
    return normalBMI * .9;
}

@synthesize employeeID;

@end