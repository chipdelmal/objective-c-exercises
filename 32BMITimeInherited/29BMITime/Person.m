//
//  Person.m
//  29BMITime
//
//  Created by Hector Sanchez on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize heightInMeters, weightInKilos;

//Operation Methods
-(float) calculateBodyMassIndex
{
    return weightInKilos / (heightInMeters * heightInMeters);
}


//Setter Methods
/*-(void) setHeightInMeters:(float)h
 {
 heightInMeters = h;
 }
 
 -(void) setWeightInKilos:(float)w
 {
 weightInKilos = w;
 }*/

//Getter Methods
/*-(float) heightInMeters
 {
 return heightInMeters;
 }
 
 -(float) weightInKilos
 {
 return weightInKilos;
 }*/

@end
