//
//  Employee.h
//  29BMITime
//
//  Created by Hector Sanchez on 10/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Person.h"

@interface Employee : Person
{
    int employeeID;
}
@property int employeeID;
@end