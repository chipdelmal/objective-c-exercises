//
//  main.c
//  13GettingAddresses
//
//  Created by Hector Sanchez on 10/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>

int main (int argc, const char * argv[])
{
    int i=17;
    int *addressOfI = &i;
    
    printf("main starts at %p\n", main);
    printf("i starts at %p\n", &i);
    printf("i stores its value in %p\n", &i);
    printf("addresOfI = %p\n", addressOfI);

    printf("the value stored at addresOfI is: %d \n", *addressOfI);
    *addressOfI=89;
    printf("the value is now: %d\n", *addressOfI);
    
    printf("an int is %zu bytes\n", sizeof(int));
    printf("a pointer is %zu bytes\n",sizeof(int *));
    
    printf("i is %zu bytes\n", sizeof(i));
    printf("address pointer is %zu bytes\n",sizeof(addressOfI));
        
    return 0;
}

